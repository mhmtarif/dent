defmodule Dent.Repo.Migrations.CreateContracts do
  use Ecto.Migration

  def change do
    create table(:contracts) do
      add :description, :text
      add :fee, :integer
      add :doctor_id, references(:users, on_delete: :nothing)
      add :patient_id, references(:patients, on_delete: :nothing)

      timestamps()
    end

    create index(:contracts, [:doctor_id])
    create index(:contracts, [:patient_id])
  end
end
