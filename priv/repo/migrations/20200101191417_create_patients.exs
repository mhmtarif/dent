defmodule Dent.Repo.Migrations.CreatePatients do
  use Ecto.Migration

  def change do
    create table(:patients) do
      add :identity_number, :string
      add :name, :string
      add :surname, :string
      add :phone, :string
      add :email, :string
      add :birth_date, :naive_datetime
      add :doctor_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:patients, [:doctor_id])
  end
end
