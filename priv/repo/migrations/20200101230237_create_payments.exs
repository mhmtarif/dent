defmodule Dent.Repo.Migrations.CreatePayments do
  use Ecto.Migration

  def change do
    create table(:payments) do
      add :miktar, :integer
      add :doctor_id, references(:users, on_delete: :nothing)
      add :patient_id, references(:patients, on_delete: :nothing)

      timestamps()
    end

    create index(:payments, [:doctor_id])
    create index(:payments, [:patient_id])
  end
end
