defmodule Dent.PatientsTest do
  use Dent.DataCase

  alias Dent.Patients

  describe "patients" do
    alias Dent.Patients.Patient

    @valid_attrs %{birth_date: ~N[2010-04-17 14:00:00], email: "some email", identity_number: "some identity_number", name: "some name", phone: "some phone", surname: "some surname"}
    @update_attrs %{birth_date: ~N[2011-05-18 15:01:01], email: "some updated email", identity_number: "some updated identity_number", name: "some updated name", phone: "some updated phone", surname: "some updated surname"}
    @invalid_attrs %{birth_date: nil, email: nil, identity_number: nil, name: nil, phone: nil, surname: nil}

    def patient_fixture(attrs \\ %{}) do
      {:ok, patient} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Patients.create_patient()

      patient
    end

    test "paginate_patients/1 returns paginated list of patients" do
      for _ <- 1..20 do
        patient_fixture()
      end

      {:ok, %{patients: patients} = page} = Patients.paginate_patients(%{})

      assert length(patients) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_patients/0 returns all patients" do
      patient = patient_fixture()
      assert Patients.list_patients() == [patient]
    end

    test "get_patient!/1 returns the patient with given id" do
      patient = patient_fixture()
      assert Patients.get_patient!(patient.id) == patient
    end

    test "create_patient/1 with valid data creates a patient" do
      assert {:ok, %Patient{} = patient} = Patients.create_patient(@valid_attrs)
      assert patient.birth_date == ~N[2010-04-17 14:00:00]
      assert patient.email == "some email"
      assert patient.identity_number == "some identity_number"
      assert patient.name == "some name"
      assert patient.phone == "some phone"
      assert patient.surname == "some surname"
    end

    test "create_patient/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Patients.create_patient(@invalid_attrs)
    end

    test "update_patient/2 with valid data updates the patient" do
      patient = patient_fixture()
      assert {:ok, patient} = Patients.update_patient(patient, @update_attrs)
      assert %Patient{} = patient
      assert patient.birth_date == ~N[2011-05-18 15:01:01]
      assert patient.email == "some updated email"
      assert patient.identity_number == "some updated identity_number"
      assert patient.name == "some updated name"
      assert patient.phone == "some updated phone"
      assert patient.surname == "some updated surname"
    end

    test "update_patient/2 with invalid data returns error changeset" do
      patient = patient_fixture()
      assert {:error, %Ecto.Changeset{}} = Patients.update_patient(patient, @invalid_attrs)
      assert patient == Patients.get_patient!(patient.id)
    end

    test "delete_patient/1 deletes the patient" do
      patient = patient_fixture()
      assert {:ok, %Patient{}} = Patients.delete_patient(patient)
      assert_raise Ecto.NoResultsError, fn -> Patients.get_patient!(patient.id) end
    end

    test "change_patient/1 returns a patient changeset" do
      patient = patient_fixture()
      assert %Ecto.Changeset{} = Patients.change_patient(patient)
    end
  end
end
