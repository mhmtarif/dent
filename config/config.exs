# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :dent,
  ecto_repos: [Dent.Repo]

# Configures the endpoint
config :dent, DentWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mG8IZrvxmk2ed2p8fwoRV7mRQ7lGCph5vNycgGb6pSPQWc6nHlm5s4pj5urMSoVE",
  render_errors: [view: DentWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Dent.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]


config :dent, :pow,
  user: Dent.Users.User,
  repo: Dent.Repo

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :torch,
  otp_app: :dent,
  template_format: "eex" || "slim"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
