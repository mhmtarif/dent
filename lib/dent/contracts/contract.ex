defmodule Dent.Contracts.Contract do
  use Ecto.Schema
  import Ecto.Changeset

  schema "contracts" do
    field :description, :string
    field :fee, :integer
    field :doctor_id, :id
    field :patient_id, :id

    timestamps()
  end

  @doc false
  def changeset(contract, attrs) do
    contract
    |> cast(attrs, [:description, :fee , :doctor_id ,:patient_id])
    |> validate_required([:description, :fee])
  end
end
