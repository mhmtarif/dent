defmodule Dent.Patients do
  @moduledoc """
  The Patients context.
  """

  import Ecto.Query, warn: false
  alias Dent.Repo
import Torch.Helpers, only: [sort: 1, paginate: 4]
import Filtrex.Type.Config

alias Dent.Patients.Patient

@pagination [page_size: 15]
@pagination_distance 5

@doc """
Paginate the list of patients using filtrex
filters.

## Examples

    iex> list_patients(%{})
    %{patients: [%Patient{}], ...}
"""
@spec paginate_patients(map) :: {:ok, map} | {:error, any}
def paginate_patients(params \\ %{} , doctor_id) do
  params =
    params
    |> Map.put_new("sort_direction", "desc")
    |> Map.put_new("sort_field", "inserted_at")

  {:ok, sort_direction} = Map.fetch(params, "sort_direction")
  {:ok, sort_field} = Map.fetch(params, "sort_field")

  with {:ok, filter} <- Filtrex.parse_params(filter_config(:patients), params["patient"] || %{}),
       %Scrivener.Page{} = page <- do_paginate_patients(filter, params ,doctor_id) do
    {:ok,
      %{
        patients: page.entries,
        page_number: page.page_number,
        page_size: page.page_size,
        total_pages: page.total_pages,
        total_entries: page.total_entries,
        distance: @pagination_distance,
        sort_field: sort_field,
        sort_direction: sort_direction
      }
    }
  else
    {:error, error} -> {:error, error}
    error -> {:error, error}
  end
end

defp do_paginate_patients(filter, params,doctor_id) do

     query = from u in Patient,
          where: u.doctor_id == ^doctor_id


  #Patient
  query
  |> Filtrex.query(filter)
  |> order_by(^sort(params))
  |> paginate(Repo, params, @pagination)
end

@doc """
Returns the list of patients.

## Examples

    iex> list_patients()
    [%Patient{}, ...]

"""
def list_patients do
  Repo.all(Patient)
end

@doc """
Gets a single patient.

Raises `Ecto.NoResultsError` if the Patient does not exist.

## Examples

    iex> get_patient!(123)
    %Patient{}

    iex> get_patient!(456)
    ** (Ecto.NoResultsError)

"""
def get_patient!(id), do: Repo.get!(Patient, id)

@doc """
Creates a patient.

## Examples

    iex> create_patient(%{field: value})
    {:ok, %Patient{}}

    iex> create_patient(%{field: bad_value})
    {:error, %Ecto.Changeset{}}

"""
def create_patient(attrs \\ %{}) do
  %Patient{}
  |> Patient.changeset(attrs)
  |> Repo.insert()
end

@doc """
Updates a patient.

## Examples

    iex> update_patient(patient, %{field: new_value})
    {:ok, %Patient{}}

    iex> update_patient(patient, %{field: bad_value})
    {:error, %Ecto.Changeset{}}

"""
def update_patient(%Patient{} = patient, attrs) do
  patient
  |> Patient.changeset(attrs)
  |> Repo.update()
end

@doc """
Deletes a Patient.

## Examples

    iex> delete_patient(patient)
    {:ok, %Patient{}}

    iex> delete_patient(patient)
    {:error, %Ecto.Changeset{}}

"""
def delete_patient(%Patient{} = patient) do
  Repo.delete(patient)
end

@doc """
Returns an `%Ecto.Changeset{}` for tracking patient changes.

## Examples

    iex> change_patient(patient)
    %Ecto.Changeset{source: %Patient{}}

"""
def change_patient(%Patient{} = patient) do
  Patient.changeset(patient, %{})
end

defp filter_config(:patients) do
  defconfig do
    text :identity_number
      text :name
      text :surname
      text :phone
      text :email
      date :birth_date
      
  end
end
end
