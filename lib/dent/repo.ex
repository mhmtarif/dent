defmodule Dent.Repo do
  use Ecto.Repo,
    otp_app: :dent,
    adapter: Ecto.Adapters.Postgres
end
