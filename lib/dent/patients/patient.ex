defmodule Dent.Patients.Patient do
  use Ecto.Schema
  import Ecto.Changeset

  schema "patients" do
    field :birth_date, :naive_datetime
    field :email, :string
    field :identity_number, :string
    field :name, :string
    field :phone, :string
    field :surname, :string
    field :doctor_id, :id

    timestamps()
  end

  @doc false
  def changeset(patient, attrs) do
    patient
    |> cast(attrs, [:identity_number, :name, :surname, :phone, :email, :birth_date, :doctor_id])
    |> validate_required([:identity_number, :name, :surname, :phone, :email, :birth_date])
  end
end
