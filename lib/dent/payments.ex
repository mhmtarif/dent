defmodule Dent.Payments do
  @moduledoc """
  The Payments context.
  """

  import Ecto.Query, warn: false
  alias Dent.Repo
import Torch.Helpers, only: [sort: 1, paginate: 4]
import Filtrex.Type.Config

alias Dent.Payments.Payment

@pagination [page_size: 15]
@pagination_distance 5

@doc """
Paginate the list of payments using filtrex
filters.

## Examples

    iex> list_payments(%{})
    %{payments: [%Payment{}], ...}
"""
#@spec paginate_payments(map) :: {:ok, map} | {:error, any}
def paginate_payments(params \\ %{},doctor_id,patient_id) do
  params =
    params
    |> Map.put_new("sort_direction", "desc")
    |> Map.put_new("sort_field", "inserted_at")

  {:ok, sort_direction} = Map.fetch(params, "sort_direction")
  {:ok, sort_field} = Map.fetch(params, "sort_field")

  with {:ok, filter} <- Filtrex.parse_params(filter_config(:payments), params["payment"] || %{}),
       %Scrivener.Page{} = page <- do_paginate_payments(filter, params,doctor_id,patient_id) do
    {:ok,
      %{
        payments: page.entries,
        page_number: page.page_number,
        page_size: page.page_size,
        total_pages: page.total_pages,
        total_entries: page.total_entries,
        distance: @pagination_distance,
        sort_field: sort_field,
        sort_direction: sort_direction
      }
    }
  else
    {:error, error} -> {:error, error}
    error -> {:error, error}
  end
end

defp do_paginate_payments(filter, params,doctor_id,patient_id) do

         query = from u in  Payment,
          where: u.doctor_id == ^doctor_id and u.patient_id==^patient_id

  query
  |> Filtrex.query(filter)
  |> order_by(^sort(params))
  |> paginate(Repo, params, @pagination)
end

@doc """
Returns the list of payments.

## Examples

    iex> list_payments()
    [%Payment{}, ...]

"""
def list_payments do
  Repo.all(Payment)
end

@doc """
Gets a single payment.

Raises `Ecto.NoResultsError` if the Payment does not exist.

## Examples

    iex> get_payment!(123)
    %Payment{}

    iex> get_payment!(456)
    ** (Ecto.NoResultsError)

"""
def get_payment!(id), do: Repo.get!(Payment, id)

@doc """
Creates a payment.

## Examples

    iex> create_payment(%{field: value})
    {:ok, %Payment{}}

    iex> create_payment(%{field: bad_value})
    {:error, %Ecto.Changeset{}}

"""
def create_payment(attrs \\ %{}) do
  %Payment{}
  |> Payment.changeset(attrs)
  |> Repo.insert()
end

@doc """
Updates a payment.

## Examples

    iex> update_payment(payment, %{field: new_value})
    {:ok, %Payment{}}

    iex> update_payment(payment, %{field: bad_value})
    {:error, %Ecto.Changeset{}}

"""
def update_payment(%Payment{} = payment, attrs) do
  payment
  |> Payment.changeset(attrs)
  |> Repo.update()
end

@doc """
Deletes a Payment.

## Examples

    iex> delete_payment(payment)
    {:ok, %Payment{}}

    iex> delete_payment(payment)
    {:error, %Ecto.Changeset{}}

"""
def delete_payment(%Payment{} = payment) do
  Repo.delete(payment)
end

@doc """
Returns an `%Ecto.Changeset{}` for tracking payment changes.

## Examples

    iex> change_payment(payment)
    %Ecto.Changeset{source: %Payment{}}

"""
def change_payment(%Payment{} = payment) do
  Payment.changeset(payment, %{})
end

defp filter_config(:payments) do
  defconfig do
    number :miktar
      
  end
end

def get_sum_of_payments(doctor_id,patient_id) do
       query = from u in  Payment,
          where: u.doctor_id == ^doctor_id and u.patient_id==^patient_id , select: sum(u.miktar)
       res=Repo.one(query)
       if res do
         res
       else
         0
       end
end

end
