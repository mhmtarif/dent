defmodule Dent.Contracts do
  @moduledoc """
  The Contracts context.
  """

  import Ecto.Query, warn: false
  alias Dent.Repo
import Torch.Helpers, only: [sort: 1, paginate: 4]
import Filtrex.Type.Config

alias Dent.Contracts.Contract

@pagination [page_size: 15]
@pagination_distance 5

@doc """
Paginate the list of contracts using filtrex
filters.

## Examples

    iex> list_contracts(%{})
    %{contracts: [%Contract{}], ...}
"""
#@spec paginate_contracts(map) :: {:ok, map} | {:error, any}
def paginate_contracts(params \\ %{},doctor_id,patient_id) do
  params =
    params
    |> Map.put_new("sort_direction", "desc")
    |> Map.put_new("sort_field", "inserted_at")

  {:ok, sort_direction} = Map.fetch(params, "sort_direction")
  {:ok, sort_field} = Map.fetch(params, "sort_field")

  with {:ok, filter} <- Filtrex.parse_params(filter_config(:contracts), params["contract"] || %{}),
       %Scrivener.Page{} = page <- do_paginate_contracts(filter, params,doctor_id,patient_id) do
    {:ok,
      %{
        contracts: page.entries,
        page_number: page.page_number,
        page_size: page.page_size,
        total_pages: page.total_pages,
        total_entries: page.total_entries,
        distance: @pagination_distance,
        sort_field: sort_field,
        sort_direction: sort_direction
      }
    }
  else
    {:error, error} -> {:error, error}
    error -> {:error, error}
  end
end

defp do_paginate_contracts(filter, params,doctor_id,patient_id) do
       query = from u in  Contract,
          where: u.doctor_id == ^doctor_id and u.patient_id==^patient_id

  #Contract
  query
  |> Filtrex.query(filter)
  |> order_by(^sort(params))
  |> paginate(Repo, params, @pagination)
end

@doc """
Returns the list of contracts.

## Examples

    iex> list_contracts()
    [%Contract{}, ...]

"""
def list_contracts do
  Repo.all(Contract)
end

@doc """
Gets a single contract.

Raises `Ecto.NoResultsError` if the Contract does not exist.

## Examples

    iex> get_contract!(123)
    %Contract{}

    iex> get_contract!(456)
    ** (Ecto.NoResultsError)

"""
def get_contract!(id), do: Repo.get!(Contract, id)

@doc """
Creates a contract.

## Examples

    iex> create_contract(%{field: value})
    {:ok, %Contract{}}

    iex> create_contract(%{field: bad_value})
    {:error, %Ecto.Changeset{}}

"""
def create_contract(attrs \\ %{}) do
  %Contract{}
  |> Contract.changeset(attrs)
  |> Repo.insert()
end

@doc """
Updates a contract.

## Examples

    iex> update_contract(contract, %{field: new_value})
    {:ok, %Contract{}}

    iex> update_contract(contract, %{field: bad_value})
    {:error, %Ecto.Changeset{}}

"""
def update_contract(%Contract{} = contract, attrs) do
  contract
  |> Contract.changeset(attrs)
  |> Repo.update()
end

@doc """
Deletes a Contract.

## Examples

    iex> delete_contract(contract)
    {:ok, %Contract{}}

    iex> delete_contract(contract)
    {:error, %Ecto.Changeset{}}

"""
def delete_contract(%Contract{} = contract) do
  Repo.delete(contract)
end

@doc """
Returns an `%Ecto.Changeset{}` for tracking contract changes.

## Examples

    iex> change_contract(contract)
    %Ecto.Changeset{source: %Contract{}}

"""
def change_contract(%Contract{} = contract) do
  Contract.changeset(contract, %{})
end

defp filter_config(:contracts) do
  defconfig do
    text :description
      number :fee
      
  end
end

def get_sum_of_contracts(doctor_id,patient_id) do
       query = from u in  Contract,
          where: u.doctor_id == ^doctor_id and u.patient_id==^patient_id , select: sum(u.fee)

       res=Repo.one(query)
       if res do
         res
       else
         0
       end
end


end
