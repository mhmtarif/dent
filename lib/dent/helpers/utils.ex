defmodule  Dent.Helpers.Utils do

  import Ecto.Query, warn: false


  def get_doctor_id(conn) do
    conn.assigns.current_user.id
  end



  def add_doctor_id(user_params, conn) do
    Map.put(user_params, "doctor_id", get_doctor_id(conn))
  end


  def get_model(model, id, conn,preload \\ []) do
    doctor_id=get_doctor_id(conn)
    query = from u in model,
                 where: u.doctor_id == ^doctor_id,
                 where: u.id == ^id



    res=Dent.Repo.one!(query)

    if !Enum.empty?(preload) do
      Enum.reduce(preload,res,fn x,acc-> Dent.Repo.preload(acc,x) end)
    else
      res
    end

  end


    def get_model2(model, id, doctor_id,preload \\ []) do

    query = from u in model,
                 where: u.doctor_id == ^doctor_id,
                 where: u.id == ^id



    res=Dent.Repo.one!(query)

    if !Enum.empty?(preload) do
      Enum.reduce(preload,res,fn x,acc-> Dent.Repo.preload(acc,x) end)
    else
      res
    end

  end



end