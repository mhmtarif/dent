defmodule Dent.Payments.Payment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "payments" do
    field :miktar, :integer
    field :doctor_id, :id
    field :patient_id, :id

    timestamps()
  end

  @doc false
  def changeset(payment, attrs) do
    payment
    |> cast(attrs, [:miktar ,:doctor_id ,:patient_id])
    |> validate_required([:miktar])
  end
end
