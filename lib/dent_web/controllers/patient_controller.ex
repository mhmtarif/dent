defmodule DentWeb.PatientController do
  use DentWeb, :controller

  alias Dent.Patients
  alias Dent.Patients.Patient
  alias Dent.Helpers.Utils

  plug(:put_layout, {DentWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    case Patients.paginate_patients(params,Utils.get_doctor_id(conn)) do
      {:ok, assigns} ->
        render(conn, "index.html", assigns)
      error ->
        conn
        |> put_flash(:error, "There was an error rendering Patients. #{inspect(error)}")
        |> redirect(to: Routes.patient_path(conn, :index))
    end
  end

  def new(conn, _params) do
    changeset = Patients.change_patient(%Patient{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"patient" => patient_params}) do
    case Patients.create_patient(Utils.add_doctor_id(patient_params,conn)) do
      {:ok, patient} ->
        conn
        |> put_flash(:info, "Patient created successfully.")
        |> redirect(to: Routes.patient_path(conn, :show, patient))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    patient= Utils.get_model(Patient,id,conn)
    #patient = Patients.get_patient!(id)
    total_payments=Dent.Payments.get_sum_of_payments(Utils.get_doctor_id(conn),patient.id)
    total_contracts=Dent.Contracts.get_sum_of_contracts(Utils.get_doctor_id(conn),patient.id)
    kalan_borc= total_contracts-total_payments
    render(conn, "show.html", patient: patient , total_payments: total_payments , total_contracts: total_contracts , kalan_borc: kalan_borc )
  end

  def edit(conn, %{"id" => id}) do
    #patient = Patients.get_patient!(id)
    patient= Utils.get_model(Patient,id,conn)
    changeset = Patients.change_patient(patient)
    render(conn, "edit.html", patient: patient, changeset: changeset)
  end

  def update(conn, %{"id" => id, "patient" => patient_params}) do
    #patient = Patients.get_patient!(id)
    patient= Utils.get_model(Patient,id,conn)

    case Patients.update_patient(patient, patient_params) do
      {:ok, patient} ->
        conn
        |> put_flash(:info, "Patient updated successfully.")
        |> redirect(to: Routes.patient_path(conn, :show, patient))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", patient: patient, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    patient= Utils.get_model(Patient,id,conn)
    #patient = Patients.get_patient!(id)
    {:ok, _patient} = Patients.delete_patient(patient)

    conn
    |> put_flash(:info, "Patient deleted successfully.")
    |> redirect(to: Routes.patient_path(conn, :index))
  end
end