defmodule DentWeb.PaymentController do
  use DentWeb, :controller

  alias Dent.Payments
  alias Dent.Payments.Payment
  alias Dent.Helpers.Utils

  plug(:put_layout, {DentWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    patient_id=Map.get(params,"patient_id")
    case Payments.paginate_payments(params,Utils.get_doctor_id(conn),patient_id) do
      {:ok, assigns} ->
        render(conn, "index.html", Map.put(assigns , "patient_id", patient_id))
      error ->
        conn
        |> put_flash(:error, "There was an error rendering Payments. #{inspect(error)}")
        |> redirect(to: Routes.patient_payment_path(conn, :index, patient_id: patient_id))
    end
  end

  def new(conn, params) do
    patient_id=Map.get(params,"patient_id")
    changeset = Payments.change_payment(%Payment{patient_id: patient_id})
    render(conn, "new.html", changeset: changeset  ,patient_id: patient_id)
  end

  def create(conn, %{"payment" => payment_params ,"patient_id" => patient_id}) do
    case Payments.create_payment(Map.put(Utils.add_doctor_id(payment_params,conn),"patient_id",patient_id )) do
      {:ok, payment} ->
        conn
        |> put_flash(:info, "Payment created successfully.")
        |> redirect(to: Routes.patient_payment_path(conn, :show, patient_id ,payment))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id ,"patient_id" => patient_id}) do
    #payment = Payments.get_payment!(id)
    payment= Utils.get_model(Payment,id,conn)
    render(conn, "show.html", payment: payment , id: id , patient_id: patient_id)
  end

  def edit(conn, %{"id" => id ,"patient_id" => patient_id}) do
    #payment = Payments.get_payment!(id)
    payment= Utils.get_model(Payment,id,conn)
    changeset = Payments.change_payment(payment)
    render(conn, "edit.html", payment: payment, changeset: changeset , id: id , patient_id: patient_id)
  end

  def update(conn, %{"id" => id, "payment" => payment_params  ,"patient_id" => patient_id}) do
    #payment = Payments.get_payment!(id)
    payment= Utils.get_model(Payment,id,conn)

    case Payments.update_payment(payment, payment_params) do
      {:ok, payment} ->
        conn
        |> put_flash(:info, "Payment updated successfully.")
        |> redirect(to: Routes.patient_payment_path(conn, :show, patient_id, payment))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", payment: payment, changeset: changeset)
    end
  end

  def delete(conn, %{"patient_id" => patient_id,"id" => id}) do
    #payment = Payments.get_payment!(id)
    payment= Utils.get_model(Payment,id,conn)
    {:ok, _payment} = Payments.delete_payment(payment)

    conn
    |> put_flash(:info, "Payment deleted successfully.")
    |> redirect(to: Routes.patient_payment_path(conn, :index ,patient_id ))
  end
end