defmodule DentWeb.PageController do
  use DentWeb, :controller
  plug :put_layout, "torch.html"

  def index(conn, _params) do
    IO.inspect conn.assigns.current_user
    render(conn, "index.html")
  end
end
