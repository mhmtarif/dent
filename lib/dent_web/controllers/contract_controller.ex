defmodule DentWeb.ContractController do
  use DentWeb, :controller

  alias Dent.Contracts
  alias Dent.Contracts.Contract
  alias Dent.Helpers.Utils

  plug(:put_layout, {DentWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    patient_id=Map.get(params,"patient_id")
    case Contracts.paginate_contracts(params,Utils.get_doctor_id(conn),patient_id) do
      {:ok, assigns} ->
        render(conn, "index.html", Map.put(assigns , "patient_id", patient_id))
      error ->
        conn
        |> put_flash(:error, "There was an error rendering Contracts. #{inspect(error)}")
        |> redirect(to: Routes.patient_contract_path(conn, :index , patient_id: patient_id))
    end
  end

  def new(conn, params) do
    patient_id=Map.get(params,"patient_id")
    changeset = Contracts.change_contract(%Contract{patient_id: patient_id})
    render(conn, "new.html", changeset: changeset ,patient_id: patient_id)
  end

  def create(conn, %{"contract" => contract_params  ,"patient_id" => patient_id}) do
    case Contracts.create_contract(Map.put(Utils.add_doctor_id(contract_params,conn),"patient_id",patient_id )) do
      {:ok, contract} ->
        conn
        |> put_flash(:info, "Contract created successfully.")
        |> redirect(to: Routes.patient_contract_path(conn, :show, patient_id,contract))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id ,"patient_id" => patient_id}) do
    contract= Utils.get_model(Contract,id,conn)
    #contract = Contracts.get_contract!(id)
    render(conn, "show.html", contract: contract , id: id , patient_id: patient_id)
  end

  def edit(conn,  %{"id" => id ,"patient_id" => patient_id}) do
    #contract = Contracts.get_contract!(id)
    contract= Utils.get_model(Contract,id,conn)
    changeset = Contracts.change_contract(contract)
    render(conn, "edit.html", contract: contract, changeset: changeset , id: id , patient_id: patient_id)
  end

  def update(conn, %{"id" => id, "contract" => contract_params ,"patient_id" => patient_id}) do
    #contract = Contracts.get_contract!(id)
    contract= Utils.get_model(Contract,id,conn)

    case Contracts.update_contract(contract, contract_params) do
      {:ok, contract} ->
        conn
        |> put_flash(:info, "Contract updated successfully.")
        |> redirect(to: Routes.patient_contract_path(conn, :show, patient_id,contract))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contract: contract, changeset: changeset)
    end
  end

  def delete(conn, %{"patient_id" => patient_id, "id" => id }) do
    #contract = Contracts.get_contract!(id)

    contract= Utils.get_model(Contract,id,conn)
    res = Contracts.delete_contract(contract)
    conn
    |> put_flash(:info, "Contract deleted successfully.")
    |> redirect(to: Routes.patient_path(conn, :show, patient_id))
  end
end